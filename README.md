# Quarto: Create Dada2 Reference Database

This is a quarto notebook for running CRABS ([Gert-Jan Jeunen et al., 2022](https://doi.org/10.1111/1755-0998.13741)) command lines on RStudio, in order to create a reference database that can be used with DADA2 ([Benjamin J Callahan et al., 2016](https://doi.org/10.1038/nmeth.3869)).

## How to build the database ?

1. Open `create_dada2_db_w_crabs.Rproj` in RStudio.

2. If it's not install already,install the `{renv}` package.

```
install.packages("renv")
```

3. Run `renv::restore()` in the R console to install all the required packages for this project.

4. Run all the code chunks in the quarto file [create_dada2_db_w_crabs.qmd](create_dada2_db_w_crabs.qmd). 

5. All done ! You've created your database.

## Licence

All code is licensed under the [MIT Licence](LICENSE.md)
